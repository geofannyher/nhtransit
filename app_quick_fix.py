"""
Copyright (c) 2023 Nippon Koei 
Do not distribute without permission

25 Jan 2023 - Published, author andyw_apcd@hotmail.com
7  Feb 2023 - Simplified routes to /hntransit, /hndriving, /driving, /transit, /housing, /nkroute_viz 

 9 Feb 2023 - app_quick_fix.py (rename to app.py to use ) to workaround async/await keywords


"""

from flask import Flask, render_template, url_for, request, redirect, jsonify, json
from datetime import datetime

import asyncio

from gmaps import nkroute_devtest2, nkroute_loadfile



app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def index():
  return render_template('index.html')



# The main endpoint for nkroute as of 10Dec2022
# 
@app.route('/nkroute_dev', methods=['GET'])
def nkroute_dev():
  if request.method == 'GET':  
    start = request.args.get('start')
    end   = request.args.get('end')
    mode  = request.args.get('mode')  
    dp_time = request.args.get('departure_time')    
    print('app.py Args: ', start, end, mode, dp_time)
    
    if mode != None or start != None or end != None :

      geojson_obj = nkroute_devtest2(start, end, mode, dp_time)
      return geojson_obj


    else:      
      return jsonify(message='GET args incomplete')

  else:
    return jsonify(message='Only GET is supported')


@app.route('/nkroute_file', methods=['GET'])
def nkroute_file():
  if request.method == 'GET':  
    filename = request.args.get('filename')
    if filename != None:
      return nkroute_loadfile(filename)
    else:      
      return jsonify(message='GET args incomplete')

  else:
    return jsonify(message='Only GET is supported')


@app.route('/nkroute_viz', methods=['GET'])
def nkroute_viz():
  return render_template('nkroute-viz10.html')

@app.route('/transit', methods=['GET'])
def transit():
  return render_template('nkroute-viz12-transit-v2.html')

@app.route('/driving', methods=['GET'])
async def driving():
  return render_template('nkroute-viz12-driving-v2.html')

@app.route('/housing', methods=['GET'])
def housing():
  return render_template('nkroute-viz12-housing.html')

@app.route('/hntransit', methods=['GET'])
def hntransit():
  return render_template('hntransit-affordability.html')

@app.route('/hndriving', methods=['GET'])
def hndriving():
  return render_template('hndriving-affordability.html')


# Extra visualisation
@app.route('/nkroute_viz12_transit', methods=['GET'])
def nkroute_viz12_transit():
  return render_template('nkroute-viz12-transit.html')

@app.route('/nkroute_viz12_driving', methods=['GET'])
def nkroute_viz12_driving():
  return render_template('nkroute-viz12-driving.html')



if __name__ == '__main__':
  app.run(debug=True)
  #app.run(host='0.0.0.0', port=80)
