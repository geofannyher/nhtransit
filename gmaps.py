"""
Copyright (c) 2023 Nippon Koei
Do not distribute without permission

25 Jan 2023 - Published, author andyw_apcd@hotmail.com

 9 Feb 2023 - qmaps_quick_fix.py (rename to gmaps.py to use ) to workaround async/await keywords

"""

import json
from flask import jsonify

import asyncio


def nkroute_loadfile(filename):

  print(f'Filename: {filename}.geojson')
  try:
    with open(f'./gmaps_data/{filename}.geojson', 'r') as f:
      geojson_obj=json.load(f)

    return geojson_obj
  except:
    return jsonify(message=f'{filename} not found!')


def nkroute_devtest2(start, end, mode, departure_time):

    return jsonify(message='Please provide your API key to setup a live NK Route API for you')
