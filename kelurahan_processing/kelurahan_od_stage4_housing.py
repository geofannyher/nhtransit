"""
Copyright (c) 2023 Nippon Koei 
Do not distribute without permission

25 Jan 2023 - Published, author andyw_apcd@hotmail.com
 4 Feb 2023 - Added update a list of js files

"""

import json
import geojson  

from csv import writer, DictWriter, reader

# The following js files are outputs from kelurahan_od_stage3.py
#
infile_list = [
  'jakarta-pusat-rw.js',
  'jakarta-selatan-rw-jp.js'
  #'jakarta-pusat-rw-js-jb-jt-ju.js',
  #'jakarta-barat-rw-jp-js-jt-ju.js',
  #'jakarta-timur-rw-jp-js-jb-ju.js',
  #'jakarta-selatan-rw-jp-jb-jt-ju.js',
  #'jakarta-utara-rw-jp-js-jb-jt.js'
]

# Viz the current housing price data
# with pre-configured Kepler.gl 
# https://nksg-public.s3.ap-southeast-1.amazonaws.com/Neighbourhood+%2B+RW+housing+price+viz.html
#
# The following file is output from house_price_csv_extract.py
csv_infile      = 'Housing Price_out.csv'

header_csv_select = ['Latitudes','Longitude','Price (million/m2)','Address','WADMKD']

def main():

  for infile in infile_list:
    update_and_create_file(infile)

  return 

def update_and_create_file(infile):

  print(f'Input js file: {infile}')
  with open(infile) as f:
    geojs = json.load(f)

  print(f'Input CSV housing price file: {csv_infile}')
  with open(csv_infile, 'r') as f_in:
    data = reader(f_in)
    header = next(data)

    if header != None:
      for n, row in enumerate(data):       
        #print(f'{n}: {row_od}')
        row_dict = dict(zip(header_csv_select, row))

        update_geojson(geojs, row_dict, row_dict['WADMKD'], output=True)


  # Save updated geojs as new geojson file
  js_outfile = f"{infile.split('.')[0]}_out.js"
  print(f'Output JS file: {js_outfile}')
  with open(f'{js_outfile}', 'w') as f:
    geojson.dump(geojs, f)

  print('\n\nREMINDER!!!  Manually edit the JS file to a add \"var jxxxData = ...\"\n\n')
  return


def update_geojson(geojs, row_dict, kelur_to_update, output):

  idx = 0

  key_identified = False

  for i, feature in enumerate(geojs['features']):
    #print(f'{i}')  # ok, outputs 0 to 266

    for key, value in feature.items():
      #print(key, value) # ok, output type, properties, geometry

      if key == 'properties' and value['WADMKD'] == kelur_to_update:
        if output is True: print(value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'], value['WADMRW'])
        key_identified = True   

        # update H
        value['H'] = row_dict['Price (million/m2)']

        idx += 1

  print(f'{idx} keys updated')
  return

if __name__ == '__main__':
  main()