
"""
Copyright (c) 2023 Nippon Koei 
Do not distribute without permission

25 Jan 2023 - Published, author andyw_apcd@hotmail.com
31 Jan 2023 - Added geojson update and save as new file to viz on Kepler.gl

Feature:
- Take 'Housing Price.csv' and create new 'Housing Price_out.csv' with selected fields and added WADMKD
- Update geojson with housing in properties and save as new geojson file


"""

import json
import geojson  

from csv import writer, DictWriter, reader

from shapely.geometry import Point, Polygon, shape, MultiPolygon

infile_csv = 'Housing Price.csv'
header_csv = ['Latitudes','Longitude','Price (million/m2)','Number','Price (M)','Installments (Jt/Month)','Bedroom','Bathroom','Building Area (m2)','Land Area (m2)','Electricity (Watts)','Seller','Agency','Interiors','Location Name','Address','image','x','y']
header_csv_select = ['Latitudes','Longitude','Price (million/m2)','Address','WADMKD']

# Stage 3 js file to update with H cost
#infile_polygons = 'JAKARTA SELATAN RW.geojson'
infile_polygons = 'Neighbourhoods.geojson'  # all kelurahan WADMKD

outfile_csv_wadmkd = f"{infile_csv.split('.')[0]}_out.csv"

def main() -> None:

  house_price_extract()

  # geojson to viz on Kepler.gl
  # To viz Housing Price.csv in Kepler.gl, you need load the csv and then manually create a Point layer
  #create_geojson_with_housing()


  return

def house_price_extract() -> None: 
  rows = 0

  with open(infile_polygons, 'r') as f_poly:
    geojs = json.load(f_poly)

    with open(outfile_csv_wadmkd, 'w') as f_out:
      write = DictWriter(f_out, fieldnames=header_csv_select)
      write.writeheader()

      with open(infile_csv, 'r') as f:
        csv_data = reader(f)
        header = next(csv_data)

        if header != None:
          for n, row in enumerate(csv_data):
            row_dict = dict(zip(header_csv, row))
          
            rows = n # keep rows count

            point = Point(float(row_dict['Longitude']), float(row_dict['Latitudes']))          

            kelurahan = is_point_in_polygon(point, geojs)

            #print(f"{n}: {row_dict['Latitudes']}, {row_dict['Longitude']}, {row_dict['Price (million/m2)']}, {row_dict['Location Name']}, {row_dict['Address']}")
            row_dict_select = {'Latitudes':row_dict['Latitudes'], 
                                'Longitude':row_dict['Longitude'], 
                                'Price (million/m2)':row_dict['Price (million/m2)'],
                                'Address':row_dict['Address'],
                                'WADMKD':kelurahan}

            #print(f"{n}: {row_dict_select}")

            write.writerow(row_dict_select)


  print(f'Input CSV  file: {infile_csv} ({rows} rows)')  
  print(f'Output CSV file: {outfile_csv_wadmkd}')
  return

# Create geojson with housing data to viz on Kepler.gl
#
def create_geojson_with_housing():

  print(f'Input geojson file: {infile_polygons}')
  with open(infile_polygons) as f:
    geojs = json.load(f)

  print(f'Input CSV housing price file: {outfile_csv_wadmkd}')
  with open(outfile_csv_wadmkd, 'r') as f_in:
    data = reader(f_in)
    header = next(data)

    if header != None:
      for n, row in enumerate(data):       
        #print(f'{n}: {row_od}')
        row_dict = dict(zip(header_csv_select, row))

        update_geojson(geojs, row_dict, row_dict['WADMKD'], output=True)


  # Save updated geojs as new geojson file
  outfile_polygons = f"{infile_polygons.split('.')[0]}_housing.geojson"
  print(f'Output geojson file: {outfile_polygons}')
  with open(f'{outfile_polygons}', 'w') as f:
    geojson.dump(geojs, f)  

  return

def update_geojson(geojs, row_dict, kelur_to_update, output):

  idx = 0

  key_identified = False

  for i, feature in enumerate(geojs['features']):
    #print(f'{i}')  # ok, outputs 0 to 266

    for key, value in feature.items():
      #print(key, value) # ok, output type, properties, geometry

      if key == 'properties' and value['WADMKD'] == kelur_to_update:
        if output is True: print(value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'])
        key_identified = True   

        # update H
        value['H'] = row_dict['Price (million/m2)']

        idx += 1

  print(f'{idx} keys updated')
  return

def is_point_in_polygon(point, geojs) -> str:
  idx = 0
  geom_is_type_multipolygon = False
  result = kelurahan = ''

  for i, feature in enumerate(geojs['features']):
    #print(f'\n{i}')  # ok

    for key, value in feature.items():

      if key == 'geometry' and value['type'] == 'MultiPolygon':
        geom_is_type_multipolygon = True
        s = shape(value)
        if s.contains(point) is True:
          print(i, idx, kelurahan, ' contains point', point)
          result = kelurahan

      if geom_is_type_multipolygon == True and key == 'properties':
        kelurahan = value['WADMKD'] # keep a copy
        #print('multipoly and properties: ', kelurahan)
        geom_is_type_multipolygon = False  # reset flag 

      idx += 1

  return result


if __name__ == '__main__':
  main()