"""
Copyright (c) 2023 Nippon Koei 
Do not distribute without permission

25 Jan 2023 - Published, author andyw_apcd@hotmail.com
 2 Feb 2023 - Tidy in/out files in selectable sets by block commenting & uncommenting

Features:
- Stage 1: Load JAKARTA PUSAT RW.goejson & JAKARTA SELATAN RW.geojson
  and create OD list.csv
- Stage 2: Load the OD list.csv from stage 1, calls NK Route API for each of the OD lat/lng, 
  and output an OD list csv file with current date appended to the filename.
  You can control the range of OD lat/lng perform the NK Route API in stage 2.

"""

import json
import geojson  

import requests
import time
from datetime import datetime, timedelta
from urllib.parse import quote

from csv import writer, DictWriter, reader


#kab_kota_filter = 'JAKARTA PUSAT'  #  8 kecamatan, 44 kelurahan, 387 RWs    
#kab_kota_filter = 'JAKARTA SELATAN' # 10 kecamatan, 65 kelurahan, 582 RWs  
#kab_kota_filter = 'JAKARTA TIMUR'   # 10 kecamatan, 65 kelurahan, 713 RWs
#kab_kota_filter = 'JAKARTA BARAT'   #  8 kecamatan, 56 kelurahan, 590 RWs
#kab_kota_filter = 'JAKARTA UTARA'   #  6 kecamatan, 31 kelurahan, 461 RWs
#kab_kota_filter = 'KAB.ADM.KEP.SERIBU' # 2 kecamatan, 6 kelurahan, 24 RWs

# OD route rule - only do inter kab_kota route, but don't route within
# 1) Done
kab_kota_origin       = 'JAKARTA SELATAN' # 10 kecamatan, 65 kelurahan, 582 RWs 
kab_kota_destination  = 'JAKARTA PUSAT'  #  8 kecamatan, 44 kelurahan, 387 RWs 

# 2) Done
#kab_kota_origin       = 'JAKARTA BARAT'   #  8 kecamatan, 56 kelurahan, 590 RWs
#kab_kota_destination  = 'JAKARTA PUSAT'  #  8 kecamatan, 44 kelurahan, 387 RWs 

# 3) Done
#kab_kota_origin       = 'JAKARTA TIMUR'   # 10 kecamatan, 65 kelurahan, 713 RWs
#kab_kota_destination  = 'JAKARTA PUSAT'  #  8 kecamatan, 44 kelurahan, 387 RWs 

# 4) Done
#kab_kota_origin       = 'JAKARTA UTARA'   #  6 kecamatan, 31 kelurahan, 461 RWs
#kab_kota_destination  = 'JAKARTA PUSAT'  #  8 kecamatan, 44 kelurahan, 387 RWs 


######
# 5) Done
#kab_kota_origin       = 'JAKARTA SELATAN' # 10 kecamatan, 65 kelurahan, 582 RWs 
#kab_kota_destination  = 'JAKARTA BARAT'   #  8 kecamatan, 56 kelurahan, 590 RWs

# 6) Done
#kab_kota_origin       = 'JAKARTA SELATAN' # 10 kecamatan, 65 kelurahan, 582 RWs 
#kab_kota_destination  = 'JAKARTA TIMUR'   # 10 kecamatan, 65 kelurahan, 713 RWs

# 7) Done
#kab_kota_origin       = 'JAKARTA SELATAN' # 10 kecamatan, 65 kelurahan, 582 RWs 
#kab_kota_destination  = 'JAKARTA UTARA'   #  6 kecamatan, 31 kelurahan, 461 RWs

#####
# 8) Done
#kab_kota_origin       = 'JAKARTA BARAT'   #  8 kecamatan, 56 kelurahan, 590 RWs
#kab_kota_destination  = 'JAKARTA TIMUR'   # 10 kecamatan, 65 kelurahan, 713 RWs

# 9) Done
#kab_kota_origin       = 'JAKARTA BARAT'   #  8 kecamatan, 56 kelurahan, 590 RWs
#kab_kota_destination  = 'JAKARTA UTARA'   #  6 kecamatan, 31 kelurahan, 461 RWs

#####

# 10) Done
#kab_kota_origin       = 'JAKARTA TIMUR'   # 10 kecamatan, 65 kelurahan, 713 RWs
#kab_kota_destination  = 'JAKARTA UTARA'   #  6 kecamatan, 31 kelurahan, 461 RWs 

infile_origin       = f'{kab_kota_origin} RW.geojson'
infile_destination  = f'{kab_kota_destination} RW.geojson'

# For stage 1, OD (4), you have to use the following
#infile_origin       = 'JAKARTA UTARA RW fixed.geojson' # Fixed KALIBARU & PLUIT WADMRW no. with blank space to 099
#infile_destination  = f'{kab_kota_destination} RW.geojson'

# For stage 1, OD (7), (9) and (10), you have to use the following
#infile_origin       = f'{kab_kota_origin} RW.geojson'
#infile_destination  = 'JAKARTA UTARA RW fixed.geojson' # Fixed KALIBARU & PLUIT WADMRW no. with blank space to 099


# Store the OD list in a csv file
outfile = f'{kab_kota_origin} {kab_kota_destination} od list.csv'


# For stage 2, OD (6), use the following, due to mod on lat/lon for 
#  HALIM PERDANA KUSUMAH to make transit routable
#outfile = 'JAKARTA SELATAN JAKARTA TIMUR od list mod.csv'

# For stage 2, OD (8), use the following, due to mod on lat/lon for KEDAUNG KALI ANGKE 
# and HALIM PERDANA KUSUMAH to make transit routable
#outfile = 'JAKARTA BARAT JAKARTA TIMUR od list mod.csv'

# For stage 2, OD (10), use the following, 3 unroutetable OD removed but need to investigate
#outfile = 'JAKARTA TIMUR JAKARTA UTARA od list mod.csv'

header_od = ['O_OBJECTID', 'O_WADMKK', 'O_WADMKC', 'O_WADMKD', 'O_WADMRW', 'O_LAT', 'O_LNG', 'D_OBJECTID', 'D_WADMKK', 'D_WADMKC', 'D_WADMKD', 'D_WADMRW', 'D_LAT', 'D_LNG', 'T transit', 'T driving', 'T ridehail', 'CO2e transit', 'CO2e driving', 'route_details', 'H']



def main():

  # Uncomment the following to control which stage to run

  # Stage 1 - OD rules and create OD list in CSV
  print('\nStage 1')
  stage_1()

  # Stage 2 - Perform NK Route API call on the OD list, and store CSV as a new copy
  #print('\nStage 2 route and output new CSV')
  #stage_2()

  # Stage 2 refresh - Perform NK Route File API call on the OD list, and store CSV as a new copy
  #print('\nStage 2 route file and output new CSV')
  #stage_2_refresh_csv_only()

  return 

def stage_1() -> None:
  print(f'Origin     : {infile_origin}')
  with open(infile_origin, 'r') as f:
    json_orig = json.load(f)

  print(f'Destination: {infile_destination}')
  with open(infile_destination, 'r') as f:
    json_dest = json.load(f)

  create_od_csv_with_selected_rw(json_orig, json_dest, rw_num=2, output=False)  
  return  

def stage_2():
  rows = 0

  date = datetime.now().strftime("%Y%m%d-%H%M%S")
  outfile_stage2 = f'{outfile.split(".")[0]} {date}.csv'

  print(f'Input  CSV OD file: {outfile}')

  with open(outfile_stage2, 'w') as f_out:
    write = writer(f_out)

    with open(outfile, 'r') as f_in:
      data = reader(f_in)
      header = next(data)
      write.writerow(header)

      if header != None:
        for n, row in enumerate(data):
          # Use the following to control the range of OD list in the csv to route
          """
          if n > 45: # the first n rows in input csv
            rows = n+1 # keep rows count, add 1 to n because n starts at 0
            break           
          #print(f'{n}: {row}')
          route_one_od(row)
          write.writerow(row)
          """
          """
          # only want the 44 to 92 rows in input csv
          if n > 91:
            rows = n+1 # keep rows count, add 1 to n because n starts at 0
            break
          if n > 43:
            #print(f'{n}: {row}')
            route_one_od(row)
            write.writerow(row)
          """
          """
          # from 1440 rows to end in input csv
          if n > 1400: #1359: #902: #857: #769: #660: #224: #199: #190: #235: #90:
            rows = n+1 # keep rows count, add 1 to n because n starts at 0
            #print(f'{n}: {row}')
            route_one_od(row)
            write.writerow(row)
          """
          #"""
          rows = n+1 # keep rows count, add 1 to n because n starts at 0
          #print(f'{n}: {row}')
          route_one_od(row)
          write.writerow(row)
          #"""

  print(f'Output CSV OD file: {outfile_stage2} ({rows} rows)')    

  return outfile_stage2


def stage_2_refresh_csv_only():

  # Todo

  return

# stage_1() sub-routine
#
def create_od_csv_with_selected_rw(origin, destination, rw_num=2, output=False):
  idx_orig = idx_dest = 0

  orig_geom_is_type_multipolygon = dest_geom_is_type_multipolygon = False

  print(f'CSV OD list: {outfile}')
  
  with open(outfile, 'w') as f:
    write = DictWriter(f, fieldnames=header_od)
    write.writeheader()

    for i, feature in enumerate(origin['features']):
      #print(f'{i}')  # ok
      for key, value in feature.items():
        #print(f'Key: {key}, Value: {value}') # ok, output type, properties, geometry

        # ORIGIN
        # Check if type: Feature and geometry: type: MultiPolygon
        # Note: This check relies on geoJSON order key geometry before key properties
        if key == 'geometry' and value['type'] == 'MultiPolygon':
          orig_geom_is_type_multipolygon = True 

        if orig_geom_is_type_multipolygon == True and key == 'properties' and \
          value['WADMKK'] == kab_kota_origin:
          if output is True: print(value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'], ',', value['WADMRW'])
          orig_geom_is_type_multipolygon = False  # reset flag   

          # Copy only the selected rw
          if rw_num == int(value['WADMRW']):          
            # write.writerow(value) # write all fields
            # only write select fields
            origin_fields = {'O_OBJECTID':value['OBJECTID'], 
                            'O_WADMKK':value['WADMKK'], 
                            'O_WADMKC':value['WADMKC'], 
                            'O_WADMKD':value['WADMKD'], 
                            'O_WADMRW':value['WADMRW'],
                            'O_LAT':value['centroid']['lat'], 'O_LNG':value['centroid']['lng']}
            #write.writerow(origin_fields)
            idx_orig += 1

            # Now loop through the destination
            for i, feature in enumerate(destination['features']):
              #print(f'{i}')  # ok
              for key, value in feature.items():

                # DESTINATION
                # Check if type: Feature and geometry: type: MultiPolygon
                # Note: This check relies on geoJSON order key geometry before key properties
                if key == 'geometry' and value['type'] == 'MultiPolygon':
                  dest_geom_is_type_multipolygon = True 

                if dest_geom_is_type_multipolygon == True and key == 'properties' and \
                  value['WADMKK'] == kab_kota_destination:
                  if output is True: print(value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'], ',', value['WADMRW'])
                  dest_geom_is_type_multipolygon = False  # reset flag   

                  # Copy only the selected rw
                  if rw_num == int(value['WADMRW']):          
                    # write.writerow(value) # write all fields
                    # only write select fields
                    dest_fields = {'D_OBJECTID':value['OBJECTID'], 
                                    'D_WADMKK':value['WADMKK'], 
                                    'D_WADMKC':value['WADMKC'], 
                                    'D_WADMKD':value['WADMKD'], 
                                    'D_WADMRW':value['WADMRW'],
                                    'D_LAT':value['centroid']['lat'], 'D_LNG':value['centroid']['lng'],
                                    'T transit':value['T transit'],
                                    'T driving':value['T driving'],
                                    'T ridehail':value['T ridehail'],
                                    'CO2e transit':value['T ridehail'],
                                    'CO2e driving':value['CO2e driving'],
                                    'route_details':value['route_details'],
                                    'H':value['H']}
                    merge_fields = {**origin_fields, **dest_fields}
                    write.writerow(merge_fields)
                    idx_dest += 1

  print(f'Origin: {idx_orig} rows, Destination: {int(idx_dest/idx_orig)} rows, Total OD: {idx_dest} rows')
  return

# stage_2() sub-routine
#
def route_one_od(row_od):

  od = dict(zip(header_od, row_od))
  print('Origin: ', od['O_WADMKD'], od['O_WADMRW'], ', Dest: ', od['D_WADMKD'], od['D_WADMRW'])

  start = f"{od['O_LAT']},{od['O_LNG']}"
  end   = f"{od['D_LAT']},{od['D_LNG']}"
  print('Start: ', start, '; End: ', end)

  # Adjust departure_time for transit to 7am same day if <7am
  # or to next Mon 7am, if it's Fri >9pm, Sat or Sun
  dt = adjust_dp_time(datetime.now())
  #dt = datetime(2022,13,12,00,00)

  # Call NK Route API
  results = nkroute_api_call(start, end, mode='transit', departure_time=dt)

  # Test with a known filename - call NK Route API file
  #filename = 'gmaps_route_20230110-233948+0700_transit'
  #results = nkroute_api_file(quote(filename))

  print(results['features'][0]['properties']['geojson_filename'])

  # Todo: Find best way to use 'keys' instead of index numbers which will cause problems
  # when we update the header
  #header_od = [
  # index = 0
  # 'O_OBJECTID', 'O_WADMKK', 'O_WADMKC', 'O_WADMKD', 'O_WADMRW', 
  # 'O_LAT', 'O_LNG', 
  # 'D_OBJECTID', 'D_WADMKK', 'D_WADMKC', 'D_WADMKD', 'D_WADMRW', 
  # 'D_LAT', 'D_LNG', 
  #  index = 14
  # 'T transit', 'T driving', 'T ridehail', 'CO2e transit', 'CO2e driving', 'route_details', 'H']

  # route_details
  row_od[19] = results['features'][0]['properties']['geojson_filename']

  # T transit
  if results['features'][0]['properties']['journey_summary']['fare_integrated'] != 0:
    row_od[14] = results['features'][0]['properties']['journey_summary']['fare_integrated']
  else:
    row_od[14] = results['features'][0]['properties']['journey_summary']['fare_standard']

  # T driving
  row_od[15] = results['features'][0]['properties']['baseline_driving']['fuel_cost_sedan']
  # T ridehail
  row_od[16] = results['features'][0]['properties']['baseline_driving']['ridehail_cost_sedan']['gocar']
  # CO2e transit
  row_od[17] = results['features'][0]['properties']['co2_emissions']['route_co2e']['value']
  # C02e driving
  row_od[18] = results['features'][0]['properties']['co2_emissions']['route_co2e']['value'] \
    + results['features'][0]['properties']['co2_emissions']['co2e_saved']['value']

  return


def nkroute_api_call(start, end, mode, departure_time):
  
  st = time.perf_counter()

  url = f'http://localhost:5000/nkroute_dev?start={start}&end={end}&mode={mode}&departure_time={departure_time}'
  r = requests.get(url)
  #results = r.json()['features'][0]['properties']['geojson_filename']
  results = r.json()

  et = time.perf_counter()
  print('API latency: ', round((et - st)*1000, 3), ' msec' )

  return results

def nkroute_api_file(filename):

  st = time.perf_counter()

  url = f'http://localhost:5000/nkroute_file?filename={filename}'
  r = requests.get(url)
  results = r.json()
  
  et = time.perf_counter()
  print('API latency: ', round((et - st)*1000, 3), ' msec' )

  return results

# Mainly to adjust the following dp time to next Monday 7am
def adjust_dp_time(time_now) -> datetime:

  print(f'Time now is: {time_now}, dow: {time_now.weekday()}, {time_now.strftime("%A")}')

  # Check for the following conditions is true, 
  # Fri and >9pm, Sat, or Sun
  # then adjust dp time to Monday 7am
  weekday = time_now.weekday()
  if (weekday == 4 and time_now.hour > 21) or \
     weekday == 5 or \
     weekday == 6:       # 4 = Fri,  5 = Sat, 6 = Sun
    time_now = time_now.replace(hour=7)
    if weekday == 4: time_now += timedelta(days=3) # add 3 days to Mon
    if weekday == 5: time_now += timedelta(days=2) # add 2 days to Mon
    if weekday == 6: time_now += timedelta(days=1) # add 1 day to Mon

  # Mon-Fri and <7am, set to 7am
  if weekday < 5 and time_now.hour < 7:
    time_now = time_now.replace(hour=7)

  print(f'Adj.time is: {time_now}, dow: {time_now.weekday()}, {time_now.strftime("%A")}')
  return time_now


if __name__ == '__main__':
  main()