"""
Copyright (c) 2023 Nippon Koei 
Do not distribute without permission

25 Jan 2023 - Published, author andyw_apcd@hotmail.com

Features:
- Reads in polygons from a geojson file
- Filter by polygon names
- Calculate centroid of polygon
- Outputs a geojson file of the filtered list of polygons and their centroids as Points
"""

import json
import geojson

#import shapely
from shapely.geometry import shape

# Uncomment the following to choose the kab_kota to extract
kab_kota_filter = 'JAKARTA PUSAT'  #  8 kecamatan, 44 kelurahan, 387 RW     
#kab_kota_filter = 'JAKARTA SELATAN' # 10 kecamatan, 65 kelurahan, 582 RW  
#kab_kota_filter = 'JAKARTA TIMUR'   # 10 kecamatan, 65 kelurahan, 713 RW
#kab_kota_filter = 'JAKARTA BARAT'   #  8 kecamatan, 56 kelurahan, 590 RW
#kab_kota_filter = 'JAKARTA UTARA'   #  6 kecamatan, 31 kelurahan, 461 RW
#kab_kota_filter = 'KAB.ADM.KEP.SERIBU' # 2 kecamatan, 6 kelurahan, 24 RW

"""
Check
JAKARTA PUSAT select rw_num=2, 88 features, 44 RW
JAKARTA SELATAN select rw_num=2, 130 features, 65 RW
"""


# Only the output {kab_kota_filter} RW.geojson is used for subsequent stage
# {kab_kota_filter} RW centroids.geojson 
# {kab_kota_filter} RW centroids select.geojson
# are used to viz the centroid point and selected RW using Kepler.gl for this stage
#
infile = 'RW.geojson'
outfile = f'{kab_kota_filter} RW.geojson'
outfile2 = f'{kab_kota_filter} RW centroids.geojson'
outfile3 = f'{kab_kota_filter} RW centroids select.geojson'

feature_count = 0
feature_count2 = 0
feature_count3 = 0

def main():

  with open(infile) as f:
    geojs = json.load(f)

  features_out = []
  feature_count = create_geojson_with_centroid(geojs, features_out, output=False)
  with open(outfile, 'w') as f:
    geojson.dump(geojson.FeatureCollection(features_out), f)

  features_out2 = []
  feature_count2 = create_geojson_with_centroid_point(geojs, features_out2, output=False)  
  with open(outfile2, 'w') as f:
    geojson.dump(geojson.FeatureCollection(features_out2), f)

  features_out3 = []
  feature_count3 = create_geojson_with_centroid_selected_rw(geojs, features_out3, rw_num=2, output=False)  
  with open(outfile3, 'w') as f:
    geojson.dump(geojson.FeatureCollection(features_out3), f)

  print('\nSummary:')
  print(f'Input file: {infile}')
  print(f'Output file 1: {outfile}, {feature_count} features')
  print(f'Output file 2: {outfile2}, {feature_count2} features')
  print(f'Output file 3: {outfile3}, {feature_count3} features')

# Create the geojson structure 
def create_geojson_with_centroid(geojs, features_out, output):

  idx = 0

  key_identified = False

  for i, feature in enumerate(geojs['features']):
    #print(f'{i}')  # ok, outputs 0 to 2756 features for RW.geoson

    for key, value in feature.items():
      #print(key, value) # ok, output type, properties, geometry

      if key == 'properties' and value['WADMKK'] == kab_kota_filter:

        if output is True: print(value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'])
        key_identified = True   
        # Copy
        features_out.append(geojson.Feature()) 
        #features_out[idx]['properties'] = value  # copy all the values
        # ...or just copy selectively for smaller file size, e.g.
        # J. Pusat rw size from 2.7 - 2.8 MB -> 2.6 MB
        # J. Selatan rw size from 6.8 - 6.8 MB -> 6.5-6.6 MB
        features_out[idx]['properties']['OBJECTID'] = value['OBJECTID']
        features_out[idx]['properties']['WADMKK'] = value['WADMKK']
        features_out[idx]['properties']['WADMKC'] = value['WADMKC']
        features_out[idx]['properties']['WADMKD'] = value['WADMKD']
        features_out[idx]['properties']['WADMRW'] = value['WADMRW']

        # add T elements
        features_out[idx]['properties']['T transit'] = 0.0
        features_out[idx]['properties']['T driving'] = 0.0
        features_out[idx]['properties']['T ridehail'] = 0.0
        features_out[idx]['properties']['CO2e transit'] = 0.0
        features_out[idx]['properties']['CO2e driving'] = 0.0
        features_out[idx]['properties']['route_details'] = ' '

        # add H elements
        features_out[idx]['properties']['H'] = 0.0

        #
        features_out[idx]['properties']['from_origin'] = {}

      if key == 'geometry' and key_identified:
        key_identified = False  # reset flag
        #print(value['coordinates'])
        s = shape(value)
        p = s.centroid
        if output is True: print(' lat/lon: ', p.y, ', ', p.x)  # Print out for easy copy & paste on Google Maps
        if output is True: print(f"'lat': {p.y}, 'lng': {p.x}") # Print out for easy copy & paste to Python code

        # Add 'centroid' to 'properties'
        features_out[idx]['properties']['centroid'] = {'lat': 0.0, 'lng': 0.0}
        features_out[idx]['properties']['centroid']['lat'] = p.y 
        features_out[idx]['properties']['centroid']['lng'] = p.x

        # Copy
        features_out[idx]['geometry'] = value  
        idx += 1

  #print(f'{idx} features')
  return idx

# Create the geojson structure 
def create_geojson_with_centroid_point(geojs, features_out, output):

  idx = 0

  key_identified = False

  for i, feature in enumerate(geojs['features']):
    #print(f'{i}')  # ok, outputs 0 to 266

    for key, value in feature.items():
      #print(key, value) # ok, output type, properties, geometry

      if key == 'properties' and value['WADMKK'] == kab_kota_filter:

        if output is True: print(value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'])
        key_identified = True   
        # Copy
        features_out.append(geojson.Feature()) 
        #features_out[idx]['properties'] = value  # copy all the values
        # ...or just copy selectively for smaller file size
        features_out[idx]['properties']['OBJECTID'] = value['OBJECTID']
        features_out[idx]['properties']['WADMKK'] = value['WADMKK']
        features_out[idx]['properties']['WADMKC'] = value['WADMKC']
        features_out[idx]['properties']['WADMKD'] = value['WADMKD']
        features_out[idx]['properties']['WADMRW'] = value['WADMRW']

        # add T elements
        features_out[idx]['properties']['T transit'] = 0.0
        features_out[idx]['properties']['T driving'] = 0.0
        features_out[idx]['properties']['T ridehail'] = 0.0
        features_out[idx]['properties']['CO2e transit'] = 0.0
        features_out[idx]['properties']['CO2e driving'] = 0.0
        features_out[idx]['properties']['route_details'] = ' '

        # add H elements
        features_out[idx]['properties']['H'] = 0.0

        # add 'from_origin' the O data in OD, where the geojson file is the D data
        features_out[idx]['properties']['from_origin'] = {}

      if key == 'geometry' and key_identified:
        key_identified = False
        #print(value['coordinates'])
        s = shape(value)
        p = s.centroid
        if output is True: print(' lat/lon: ', p.y, ', ', p.x)  # Print out for easy copy & paste on Google Maps
        if output is True: print(f"'lat': {p.y}, 'lng': {p.x}") # Print out for easy copy & paste to Python code

        # Add 'centroid' to 'properties'
        features_out[idx]['properties']['centroid'] = {'lat': 0.0, 'lng': 0.0}
        features_out[idx]['properties']['centroid']['lat'] = p.y 
        features_out[idx]['properties']['centroid']['lng'] = p.x

        # Copy
        features_out[idx]['geometry'] = value  
        idx += 1

        # Create new Point feature
        s_centroid = geojson.Point((p.x, p.y)) # lon/lat order
        features_out.append(geojson.Feature(geometry=s_centroid)) 
        idx += 1

  #print(f'{idx} features')
  return idx


# Create the geojson structure 
def create_geojson_with_centroid_selected_rw(geojs, features_out, rw_num, output):

  idx = 0

  key_identified = rw_identified = False

  for i, feature in enumerate(geojs['features']):
    #print(f'{i}')  # ok

    for key, value in feature.items():
      #print(key, value) # ok, output type, properties, geometry

      if key == 'properties' and value['WADMKK'] == kab_kota_filter:
        if output is True: print(value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'], ',', value['WADMRW'])
        key_identified = True   

        # Copy only the selected rw
        if rw_num == int(value['WADMRW']):
          rw_identified = True
          features_out.append(geojson.Feature()) 
          #features_out[idx]['properties'] = value  # copy all the values
          # ...or just copy selectively for smaller file size
          features_out[idx]['properties']['OBJECTID'] = value['OBJECTID']
          features_out[idx]['properties']['WADMKK'] = value['WADMKK']
          features_out[idx]['properties']['WADMKC'] = value['WADMKC']
          features_out[idx]['properties']['WADMKD'] = value['WADMKD']
          features_out[idx]['properties']['WADMRW'] = value['WADMRW']

          # add T elements
          features_out[idx]['properties']['T transit'] = 0.0
          features_out[idx]['properties']['T driving'] = 0.0
          features_out[idx]['properties']['T ridehail'] = 0.0
          features_out[idx]['properties']['CO2e transit'] = 0.0
          features_out[idx]['properties']['CO2e driving'] = 0.0
          features_out[idx]['properties']['route_details'] = ' '

          # add H elements
          features_out[idx]['properties']['H'] = 0.0

          # add 'from_origin' the O data in OD, where the geojson file is the D data
          features_out[idx]['properties']['from_origin'] = {}

      if key == 'geometry' and key_identified and rw_identified:
        key_identified = rw_identified = False # reset flags
        #print(value['coordinates'])
        
        s = shape(value)
        p = s.centroid
        if output is True: print(' lat/lon: ', p.y, ', ', p.x)  # Print out for easy copy & paste on Google Maps
        if output is True: print(f"'lat': {p.y}, 'lng': {p.x}") # Print out for easy copy & paste to Python code

        # Add 'centroid' to 'properties'
        features_out[idx]['properties']['centroid'] = {'lat': 0.0, 'lng': 0.0}
        features_out[idx]['properties']['centroid']['lat'] = p.y 
        features_out[idx]['properties']['centroid']['lng'] = p.x

        # Copy only the selected rw
        features_out[idx]['geometry'] = value  
        idx += 1

        # Create new Point feature
        s_centroid = geojson.Point((p.x, p.y)) # lon/lat order
        features_out.append(geojson.Feature(geometry=s_centroid)) 
        idx += 1

  #print(f'{idx} features')
  return idx


if __name__ == '__main__':
  main()