"""
Copyright (c) 2023 Nippon Koei 
Do not distribute without permission

25 Jan 2023 - Published, author andyw_apcd@hotmail.com
 2 Feb 2023 - Tidy in/out files in selectable sets by block commenting & uncommenting
            - Merge kelurahan_od_stage3.py and kelurahan_od_stage3_reverse.py into single file

"""

import json
import geojson  

from csv import reader

# If you are outputing the js first in the first time, 
# follow the order below because some outputs relies on previous updated files
#

# 1) JS to JP for OD > JP-js; JP to JS for DO > JS-jp
#"""
kab_kota_origin       = 'JAKARTA SELATAN' # 10 kecamatan, 65 kelurahan, 582 RWs 
kab_kota_destination  = 'JAKARTA PUSAT'  #  8 kecamatan, 44 kelurahan, 387 RWs 
infile_csv            = 'JAKARTA SELATAN JAKARTA PUSAT od list 20230207 merge keep.csv' #  2860 rows - 1 row missing, total 2884 rows w/ row 2-24 are duplicates and can be ignored
infile_origin         = f'{kab_kota_origin} RW.geojson'

infile_destination    = f'{kab_kota_destination} RW.geojson'  # JP file has no OD data
outfile_js_for_od     = f"{infile_destination.split('.')[0].lower().replace(' ', '-')}-js.js" # Output updated dest_geojs as the first JP file with .js extention

outfile_js_for_do     = f"{infile_origin.split('.')[0].lower().replace(' ', '-')}-jp.js" # Output updated orig_geojs as the first JS file with .js extention
#"""

# 2) JB to JP for OD > JP-js-jb; JP to JB for DO > JB-jp
"""
kab_kota_origin       = 'JAKARTA BARAT'  #  8 kecamatan, 56 kelurahan, 590 RWs
kab_kota_destination  = 'JAKARTA PUSAT'  #  8 kecamatan, 44 kelurahan, 387 RWs 
infile_csv            = 'JAKARTA BARAT JAKARTA PUSAT od list 20230202 merge keep.csv' # 2465 rows, check ok
infile_origin         = f'{kab_kota_origin} RW.geojson'

infile_destination    = 'jakarta-pusat-rw-js.js' # Add OD data to existing JP file
outfile_js_for_od     = f"{infile_destination.split('.')[0]}-jb.js"  # Output updated dest_geojs as file with .js extention

outfile_js_for_do     = f"{infile_origin.split('.')[0].lower().replace(' ', '-')}-jp.js" # Output updated orig_geojs as the first JB file with .js extention
"""

# 3) JT to JP for OD > JP-js-jb-jt; JP to JT for DO > JT-jp
"""
kab_kota_origin       = 'JAKARTA TIMUR'   # 10 kecamatan, 65 kelurahan, 713 RWs
kab_kota_destination  = 'JAKARTA PUSAT'  #  8 kecamatan, 44 kelurahan, 387 RWs 
infile_csv            = 'JAKARTA TIMUR JAKARTA PUSAT od list 20230203 merge keep.csv' # 2861 rows, check ok
infile_origin         = f'{kab_kota_origin} RW.geojson'

infile_destination    = 'jakarta-pusat-rw-js-jb.js' # Add OD data to existing JP file
outfile_js_for_od     = f"{infile_destination.split('.')[0]}-jt.js"  # Output updated dest_geojs as file with .js extention

outfile_js_for_do     = f"{infile_origin.split('.')[0].lower().replace(' ', '-')}-jp.js" # Output updated orig_geojs as the first JT file with .js extention
"""

# 4) JU to JP for OD > JP-js-jb-jt-ju; JP to JU for DO > JU-jp
"""
kab_kota_origin       = 'JAKARTA UTARA'   #  6 kecamatan, 31 kelurahan, 461 RWs
kab_kota_destination  = 'JAKARTA PUSAT'  #  8 kecamatan, 44 kelurahan, 387 RWs 
infile_csv            = 'JAKARTA UTARA JAKARTA PUSAT od list 20230206 keep.csv' # 1365 rows, check ok
infile_origin         = f'{kab_kota_origin} RW.geojson'

infile_destination    = 'jakarta-pusat-rw-js-jb-jt.js' # Add OD data to existing JP file
outfile_js_for_od     = f"{infile_destination.split('.')[0]}-ju.js"  # Output updated dest_geojs as file with .js extention

outfile_js_for_do     = f"{infile_origin.split('.')[0].lower().replace(' ', '-')}-jp.js" # Output updated orig_geojs as the first JU file with .js extention
"""


# 5) JS to JB for OD > JB-jp-js; JB to JS for DO > JS-jp-jb
"""
kab_kota_origin       = 'JAKARTA SELATAN' # 10 kecamatan, 65 kelurahan, 582 RWs 
kab_kota_destination  = 'JAKARTA BARAT'  #  8 kecamatan, 56 kelurahan, 590 RWs
infile_csv            = 'JAKARTA SELATAN JAKARTA BARAT od list 20230202 keep.csv' # rows 3641, check ok

infile_origin         = 'jakarta-selatan-rw-jp.js' # Add OD data to existing JS file
infile_destination    = 'jakarta-barat-rw-jp.js' # Add OD data to existing JB file
outfile_js_for_od     = f"{infile_destination.split('.')[0]}-js.js" # Save updated dest_geojs as file with .js extention

outfile_js_for_do     = f"{infile_origin.split('.')[0].lower().replace(' ', '-')}-jb.js" # Output updated orig_geojs as added OD data to existing JS file
"""

# 6) JS to JT for OD > JT-jp-js; JT to JS for DO > JS-jp-jb-jt
"""
kab_kota_origin       = 'JAKARTA SELATAN' # 10 kecamatan, 65 kelurahan, 582 RWs 
kab_kota_destination  = 'JAKARTA TIMUR'   # 10 kecamatan, 65 kelurahan, 713 RWs
infile_csv            = 'JAKARTA SELATAN JAKARTA TIMUR od list mod 20230207 keep.csv' # rows 4226, check ok

infile_origin         = 'jakarta-selatan-rw-jp-jb.js' # Add OD data to existing JS file
infile_destination    = 'jakarta-timur-rw-jp.js' # Add OD data to existing JT file
outfile_js_for_od     = f"{infile_destination.split('.')[0]}-js.js" # Save updated dest_geojs as file with .js extention

outfile_js_for_do     = f"{infile_origin.split('.')[0].lower().replace(' ', '-')}-jt.js" # Output updated orig_geojs as added OD data to existing JS file
"""

# 7) JS to JU for OD > JU-jp-js; JS to JU for DO > JS-jp-jb-jt-ju
"""
kab_kota_origin       = 'JAKARTA SELATAN' # 10 kecamatan, 65 kelurahan, 582 RWs 
kab_kota_destination  = 'JAKARTA UTARA'   #  6 kecamatan, 31 kelurahan, 461 RWs
infile_csv            = 'JAKARTA SELATAN JAKARTA UTARA od list 20230206 merge keep.csv' # rows 2016, check ok

infile_origin         = 'jakarta-selatan-rw-jp-jb-jt.js' # Add OD data to existing JS file
infile_destination    = 'jakarta-utara-rw-jp.js' # Add OD data to existing JU file
outfile_js_for_od     = f"{infile_destination.split('.')[0]}-js.js" # Save updated dest_geojs as file with .js extention

outfile_js_for_do     = f"{infile_origin.split('.')[0].lower().replace(' ', '-')}-ju.js" # Output updated orig_geojs as added OD data to existing JS file
"""

# 8) JB to JT for OD > JU-jp-js; JS to JU for DO > JS-jp-jb-jt-ju
"""
kab_kota_origin       = 'JAKARTA BARAT'   #  8 kecamatan, 56 kelurahan, 590 RWs
kab_kota_destination  = 'JAKARTA TIMUR'   # 10 kecamatan, 65 kelurahan, 713 RWs
infile_csv            = 'JAKARTA BARAT JAKARTA TIMUR od list mod 20230206 merge keep.csv' # rows 3641, check ok

infile_origin         = 'jakarta-barat-rw-jp-js.js' # Add OD data to existing JB file
infile_destination    = 'jakarta-timur-rw-jp-js.js' # Add OD data to existing JT file
outfile_js_for_od     = f"{infile_destination.split('.')[0]}-jb.js" # Save updated dest_geojs as file with .js extention

outfile_js_for_do     = f"{infile_origin.split('.')[0].lower().replace(' ', '-')}-jt.js" # Output updated orig_geojs as added OD data to existing JB file
"""

# 9) JB to JU for OD > JU-jp-js-jb; JU to JB for DO > JB-jp-js-jt-ju
"""
kab_kota_origin       = 'JAKARTA BARAT'   #  8 kecamatan, 56 kelurahan, 590 RWs
kab_kota_destination  = 'JAKARTA UTARA'   #  6 kecamatan, 31 kelurahan, 461 RWs
infile_csv            = 'JAKARTA BARAT JAKARTA UTARA od list 20230207-145307.csv' # rows 1737, check ok

infile_origin         = 'jakarta-barat-rw-jp-js-jt.js' # Add OD data to existing JB file
infile_destination    = 'jakarta-utara-rw-jp-js.js' # Add OD data to existing JU file
outfile_js_for_od     = f"{infile_destination.split('.')[0]}-jb.js" # Save updated dest_geojs as file with .js extention

outfile_js_for_do     = f"{infile_origin.split('.')[0].lower().replace(' ', '-')}-ju.js" # Output updated orig_geojs as added OD data to existing JB file
"""

# 10) JT to JU for OD > JU-jp-js-jb-jt; JT to JU for DO > JT-jp-js-jb-ju
"""
kab_kota_origin       = 'JAKARTA TIMUR'   # 10 kecamatan, 65 kelurahan, 713 RWs
kab_kota_destination  = 'JAKARTA UTARA'   #  6 kecamatan, 31 kelurahan, 461 RWs
infile_csv            = 'JAKARTA TIMUR JAKARTA UTARA od list 20230207 merge keep.csv' # rows 2013 - 3 rows removed for now due to un-routable, check ok

infile_origin         = 'jakarta-timur-rw-jp-js-jb.js' # Add OD data to existing JT file
infile_destination    = 'jakarta-utara-rw-jp-js-jb.js' # Add OD data to existing JU file
outfile_js_for_od     = f"{infile_destination.split('.')[0]}-jt.js" # Save updated dest_geojs as file with .js extention

outfile_js_for_do     = f"{infile_origin.split('.')[0].lower().replace(' ', '-')}-ju.js" # Output updated orig_geojs as added OD data to existing JB file
"""


header_od = ['O_OBJECTID', 'O_WADMKK', 'O_WADMKC', 'O_WADMKD', 'O_WADMRW', 'O_LAT', 'O_LNG', 'D_OBJECTID', 'D_WADMKK', 'D_WADMKC', 'D_WADMKD', 'D_WADMRW', 'D_LAT', 'D_LNG', 'T transit', 'T driving', 'T ridehail', 'CO2e transit', 'CO2e driving', 'route_details', 'H']

# Save updated geojs as file with .js extention
outfile_js = f"{infile_destination.split('.')[0].lower().replace(' ', '-')}.js"


def main():

  create_od_js_file()

  create_do_js_file()

  return 


def create_od_js_file():
  infile_csv_rows = 0
  num_of_keys = num_of_features = 0

  features_out = []
  feature_destination = 0

  print(f'Input geojson file: {infile_destination}')
  with open(infile_destination) as f:
    geojs = json.load(f)


  for n, features in enumerate(geojs['features'], start=1): 
    #print(features)
    num_of_features = n
  #print(f'{len(geojs)} {num_of_keys} {num_of_features}')  # 2 i.e. {'type': 'FeatureCollection', 'features': [] }
  

  print(f'Input CSV OD  file: {infile_csv}')
  with open(infile_csv, 'r') as f_in:
    csv_data = reader(f_in)
    header = next(csv_data)

    if header != None:
      for n, row_od in enumerate(csv_data, start=1):       
        #print(f'{n}: {row_od}')
        od = dict(zip(header_od, row_od))

        if od['O_WADMKK'] == kab_kota_origin:

          if od['D_WADMKK'] == kab_kota_destination:

            update_geojson_v4(geojs, od, output=True)

            feature_destination += 1

        infile_csv_rows = n

  print('\nSummary:')
  print(f'Input geojson file: {infile_destination} {num_of_features} features.')
  print(f'Input CSV OD  file: {infile_csv}  {infile_csv_rows} rows, {feature_destination} processsed.')

  print(f'Output JS     file: {outfile_js}')
  with open(f'{outfile_js}', 'w') as f:
     geojson.dump(geojs, f)

  return



def update_geojson_v4(geojs, od, output):

  idx = feature_count = 0

  kelur_destination = od['D_WADMKD']
  kelur_origin      = od['O_WADMKD']

  for i, feature in enumerate(geojs['features']):
    #print(f'{i}')  # ok, output 0 to x feature in features

    for key, value in feature.items():
      #print(key, value) # ok, output type, properties, geometry

      # Add origin kelurahan to 'from_origin' if kelurahan destination matches
      if key == 'properties' and value['WADMKD'] == kelur_destination:
        if output is True: 
          print('Destination: ', value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'], value['WADMRW'], 'Origin: ', kelur_origin )

        geojs['features'][i]['properties']['from_origin'][f"{kelur_origin}"] = \
          {'WADMRW': od['O_WADMRW'],
          'T transit': od['T transit'], 'T driving': od['T driving'], 'T ridehail': od['T ridehail'], 
          'CO2e transit': od['CO2e transit'], 'CO2e driving': od['CO2e driving'], 
          'route_details': od['route_details']}

        idx += 1 # count keys added

    feature_count = i+1 # as i starts at 0

  print(f'{idx} keys added to from_origin. {feature_count} features.')

  return

def create_do_js_file():
  infile_csv_rows = 0
  num_of_keys = num_of_features = 0

  features_out = []
  feature_origin = 0

  print(f'Input origin geojson file: {infile_origin}')  
  with open(infile_origin) as f:
    orig_geojs = json.load(f)

  # for n, key in enumerate(orig_geojs, start=1): 
  #   #print(key)
  #   num_of_keys = n

  for n, features in enumerate(orig_geojs['features'], start=1): 
    #print(features)
    num_of_features = n
  #print(f'{len(orig_geojs)} {num_of_keys} {num_of_features}')  # 2 i.e. {'type': 'FeatureCollection', 'features': [] }


  print(f'Input CSV OD  file: {infile_csv}')
  with open(infile_csv, 'r') as f_in:
    csv_data = reader(f_in)
    header = next(csv_data)

    if header != None:
      for n, row_od in enumerate(csv_data, start=1):       
        #print(f'{n}: {row_od}')
        od = dict(zip(header_od, row_od))

        if od['O_WADMKK'] == kab_kota_origin:

          if od['D_WADMKK'] == kab_kota_destination:

            update_geojson_reversed_od(orig_geojs, od, output=True)

            feature_origin += 1

        infile_csv_rows = n

  print('\nSummary:')
  print(f'Input origin geojson file: {infile_origin} {num_of_features} features.')
  print(f'Input CSV OD  file: {infile_csv}  {infile_csv_rows} rows, {feature_origin} processsed.')


  print(f'Output JS     file: {outfile_js_for_do}')
  with open(f'{outfile_js_for_do}', 'w') as f:
     geojson.dump(orig_geojs, f)

  return


def update_geojson_reversed_od(geojs, od, output):

  idx = feature_count = 0

  kelur_destination = od['D_WADMKD']
  kelur_origin      = od['O_WADMKD'] 

  for i, feature in enumerate(geojs['features']):
    #print(f'{i}')  # ok, output 0 to x feature in features

    for key, value in feature.items():
      #print(key, value) # ok, output type, properties, geometry

      # Add kelurahan to 'from_origin' if kelurahan destination matches
      if key == 'properties' and value['WADMKD'] == kelur_origin:
        if output is True: 
          #print('Destination: ', value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'], value['WADMRW'], 'Origin: ', kelur_origin )
          print('Destination: ', value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'], value['WADMRW'], 'Origin: ', kelur_destination )

        # WATCH OUT! E.g. J.SELATAN geojson (origin), 
        # we fill the 'from_origin' with the destination from CSV OD
        geojs['features'][i]['properties']['from_origin'][f"{kelur_destination}"] = \
          {'WADMRW': od['D_WADMRW'],
          'T transit': od['T transit'], 'T driving': od['T driving'], 'T ridehail': od['T ridehail'], 
          'CO2e transit': od['CO2e transit'], 'CO2e driving': od['CO2e driving'], 
          'route_details': od['route_details']}

        idx += 1 # count keys added

    feature_count = i+1 # as i starts at 0

  print(f'{idx} keys added to from_origin. {feature_count} features.')

  return

if __name__ == '__main__':
  main()