Copyright (c) 2023 Nippon Koei 
Do not distribute without permission




///////////////////////
Troubleshooting

9 Feb 2023
Issue 1: When update my local files from the google drive, then following the 'Quick start' steps and enter http://127.0.0.1:5000/nkroute_viz10 in the browser, I get error
Not Found
The requested URL was not found on the server. ...

Solution:
The URLs in v0.5 had been simplified to 
http://127.0.0.1:5000/nkroute_viz
http://127.0.0.1:5000/driving
http://127.0.0.1:5000/transit
http://127.0.0.1:5000/housing
and two new URLs added i.e.
http://127.0.0.1:5000/hntransit
http://127.0.0.1:5000/hndriving


Issue 2:
Issue 1: When update my local files from the google drive, then following the 'Quick start' steps and enter http://127.0.0.1:5000 in the browser to check the version number, I get error
Not Found
The requested URL was not found on the server. ...

Solution 1:
This is due to some strange problem with Python virtual env and async/await package, where the app.py and gmaps.py 
can't find the async/await package, and can't be solve even you run 
pip3 install "Flask[async]"
again.
If you're in (venv), run 
$ deactivate 
Then delete the venv folder, and redo the following:
$ mkdir venv 
$ python3 -m venv venv 
$ source venv/bin/activate
$ pip3 install -r requirements.txt 
$ python3 app.py


Solution 2 (quick workaround):
Delete app.py and gmaps.py 
Rename app_quick_fix.py and gmaps_quick_fix.py to app.py and gmaps.py repectively




////////////////////////
////////////////////////
8 Feb 2023 - Update code to v0.5

Quick start is the same, but the URLs have been simplified
From you browser, try the following sample interactive visualisation

http://127.0.0.1:5000/hntransit
http://127.0.0.1:5000/hndriving

http://127.0.0.1:5000/driving
http://127.0.0.1:5000/transit
http://127.0.0.1:5000/housing
http://127.0.0.1:5000/nkroute_viz


Files added:
-----------
templates/
hntransit-affordability.html
hndriving-affordability.html

static/js/
jakarta-barat-rw.js
jakarta-timur-rw.js
jakarta-utara-rw.js


kelurahan_processing/
kelurahan_od_route.py
kelurahan_od_stage3.py
kelurahan_od_stage4_housing.py

JAKARTA SELATAN JAKARTA PUSAT od list 20230207 merge keep.csv
JAKARTA BARAT JAKARTA PUSAT od list 20230202 merge keep.csv
JAKARTA TIMUR JAKARTA PUSAT od list 20230203 merge keep.csv
JAKARTA UTARA JAKARTA PUSAT od list 20230206 keep.csv
JAKARTA SELATAN JAKARTA BARAT od list 20230202 keep.csv
JAKARTA SELATAN JAKARTA TIMUR od list mod 20230207 keep.csv
JAKARTA SELATAN JAKARTA UTARA od list 20230206 merge keep.csv
JAKARTA BARAT JAKARTA TIMUR od list mod 20230206 merge keep.csv
JAKARTA BARAT JAKARTA UTARA od list 20230207-145307.csv
JAKARTA TIMUR JAKARTA UTARA od list 20230207 merge keep.csv

Files deleted:
-------------
kelurahan_processing/
kelurahan_od_stage3_reverse.py  (merged into kelurahan_od_stage3.py)


Files updated:
-------------
readme.txt
app.py 

templates/
index.html
nkroute-viz12-driving-v2.html
nkroute-viz12-transit-v2.html
nkroute-viz12-housing.html

static/js/
jakarta-pusat-rw.js
jakarta-selatan-rw.js

kelurahan_processing/
kelurahan_od_route.py
kelurahan_od_stage3.py
kelurahan_od_stage4_housing.py




////////////////////////
////////////////////////
25 Jan 2023 - Published, author andyw_apcd@hotmail.com

Read these 2 documents:
======================

NK API and H+T proj - areas of development that needed help.docx
https://docs.google.com/document/d/1c3MDt2JCKJlD37Q7vYQrO0bwNSRV8GRSbL040tk_6nI/edit?usp=sharing

NK Route API and NK H+T API preview introduction.pptx
https://docs.google.com/presentation/d/1S3JvsNLKK7LunBVRMWJ46yGWadiaJEjCSqyyzCmDd8s/edit?usp=sharing


Quick start:
============

Run the following commands in your MacOS terminal
$ mkdir NKHNT_pub
$ cd NKHNT_pub
$ mkdir venv 
$ python3 -m venv venv 
$ source venv/bin/activate
$ pip3 install -r requirements.txt 

Copy all the files and directories from here to NKHNT_pub
$ python3 app.py

Example outputs:
$ python3 app.py
 * Serving Flask app 'app'
 * Debug mode: on
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:5000
Press CTRL+C to quit
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 232-705-106


From you browser, try the following samplle interactive visualisatiion
http://127.0.0.1:5000/nkroute_viz12_driving_v2
http://127.0.0.1:5000/nkroute_viz12_transit_v2
http://127.0.0.1:5000/nkroute_viz12_housing
http://127.0.0.1:5000/nkroute_viz10


The following two has a choropleth refresh bug
http://127.0.0.1:5000/nkroute_viz12_driving
http://127.0.0.1:5000/nkroute_viz12_transit






