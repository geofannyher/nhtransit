"""
Copyright (c) 2023 Nippon Koei 
Do not distribute without permission

 9 Feb 2023 - Created, author andyw_apcd@hotmail.com
10 Feb 2023 - Fixed typo error with JAKARTA SELATAN income


Features:
- Reads kelurahan admin level Neighbourhoods.geojson file, 
  - exclude kab_kota 'KAB.ADM.KEP.SERIBU'
  - removes unused properties,
  - adds required H+T properties.


Reference:
https://www.pythoncheatsheet.org/cheatsheet/file-directory-path

"""

import json
import geojson

from pathlib import Path

infile  = 'Neighbourhoods.geojson'
outfile = f'{infile.split(".")[0]}_out1.geojson'

kab_kota_filter = 'KAB.ADM.KEP.SERIBU'

def main():

  d = Path(__file__).resolve().parents[1]
  #d = d / 'Data' / 'yourfile.txt'
  d = d / infile
  #print(d)

  with open(d, 'r') as f:
    geojs = json.load(f)

  features_out = []
  create_new_geojson(geojs, features_out, output=False)

  with open(outfile, 'w') as f:
    geojson.dump(geojson.FeatureCollection(features_out), f)

  return

# Create the geojson structure 
def create_new_geojson(geojs, features_out, output):

  idx = 0

  key_identified = False

  for i, feature in enumerate(geojs['features']):
    #print(f'{i}')  # ok, outputs 0 to 2756 features for RW.geoson

    for key, value in feature.items():
      #print(key, value) # ok, output type, properties, geometry

      if key == 'properties' and value['WADMKK'] != kab_kota_filter:
        if output is True: print(value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'])
        key_identified = True   
        # Copy
        features_out.append(geojson.Feature()) 
        #features_out[idx]['properties'] = value  # copy all the values
        # ...or just copy selectively for smaller file size, e.g.
        # J. Pusat rw size from 2.7 - 2.8 MB -> 2.6 MB
        # J. Selatan rw size from 6.8 - 6.8 MB -> 6.5 - 6.6 MB
        features_out[idx]['properties']['OBJECTID'] = value['OBJECTID']
        features_out[idx]['properties']['WADMKK'] = value['WADMKK']
        features_out[idx]['properties']['WADMKC'] = value['WADMKC']
        features_out[idx]['properties']['WADMKD'] = value['WADMKD']
        features_out[idx]['properties']['SHAPE_Area'] = value['SHAPE_Area']
        features_out[idx]['properties']['SHAPE_Length'] = value['SHAPE_Length']

        # add T elements
        features_out[idx]['properties']['T_transit'] = 0.0
        features_out[idx]['properties']['T_driving'] = 0.0
        features_out[idx]['properties']['T_ridehail'] = 0.0
        features_out[idx]['properties']['CO2e_transit'] = 0.0
        features_out[idx]['properties']['CO2e_driving'] = 0.0
        features_out[idx]['properties']['route_details'] = ' '

        # add H elements
        h_cost = get_h_cost(value['WADMKK'])
        features_out[idx]['properties']['H'] = h_cost

        # add income element
        income = get_income(value['WADMKK'])
        features_out[idx]['properties']['income'] = income
        features_out[idx]['properties']['pct_income_on_T_transit'] = 0.0    
        features_out[idx]['properties']['pct_income_on_T_driving'] = 0.0
        features_out[idx]['properties']['pct_income_on_T_ridehail'] = 0.0
        features_out[idx]['properties']['pct_income_on_H_cost'] = round((h_cost/income)*100, 2)
        features_out[idx]['properties']['pct_income_on_HnTr_cost'] = 0.0
        features_out[idx]['properties']['pct_income_on_HnTd_cost'] = 0.0
        features_out[idx]['properties']['pct_income_on_HnTh_cost'] = 0.0

        # add O elements from OD
        features_out[idx]['properties']['from_origin'] = {}

        # add D elements from OD       
        features_out[idx]['properties']['to_destination'] = ''

      if key == 'geometry' and key_identified:
        key_identified = False  # reset flag

        # Copy
        features_out[idx]['geometry'] = value  
        idx += 1


  print(f'{idx} features')
  return

# Housing cost data is from here
# https://docs.google.com/spreadsheets/d/1_Z1578U8WDMsj37SwsKl_3SBvc_8EDiq/edit?usp=sharing&ouid=104549682188001295897&rtpof=true&sd=true
#
# IDR per month
def get_h_cost(kab_kota):
  h_cost = 0

  if kab_kota == 'JAKARTA BARAT':
    h_cost = 728109.74
  elif kab_kota == 'JAKARTA TIMUR':
    h_cost = 757004.63
  elif kab_kota == 'JAKARTA UTARA':
    h_cost = 767450.40
  elif kab_kota == 'JAKARTA SELATAN':
    h_cost = 797014.78
  elif kab_kota == 'JAKARTA PUSAT':
    h_cost = 1237300.61

  return h_cost

# IDR per month
def get_income(kab_kota):
  income = 0

  if kab_kota == 'JAKARTA BARAT':
    income = 7601612.90
  elif kab_kota == 'JAKARTA TIMUR':
    income = 7601612.90
  elif kab_kota == 'JAKARTA UTARA':
    income = 7601612.90
  elif kab_kota == 'JAKARTA SELATAN':
    income = 7601612.90
  elif kab_kota == 'JAKARTA PUSAT':
    income = 7601612.90

  return income

if __name__ == '__main__':
  main()