"""
Copyright (c) 2023 Nippon Koei 
Do not distribute without permission

 9 Feb 2023 - Created, author andyw_apcd@hotmail.com


Features:
- Reads in the routed OD lists and updates the Neighbourhoods.geojson
  step by step

"""

import json
import geojson

from csv import reader
from pathlib import Path


# Scenario 1:
# Destination / Work: SCBD, kelurahan Senayan, kecamatan Kebayoran Baru, JS
# Origin / Home     : JS, JP, JU, JB, JT 
# Implies:
#  - income level uses SCBD, JS
#  - housing cost are JS, JP, JU, JB, JT
#  - transport cost is from JS, JP, JU, JB, JT to SCBD respectively


# Uncomment / comment each code block to execute in sequence
#
# NOTE!!! Change to use create_step2_geojson_file_() !!!
#""" 1)
kab_kota_origin = 'JAKARTA SELATAN'  # 10 kecamatan, 65 kelurahan, 582 RWs 
destination     = 'SETIA BUDI'       # destination: kelurahan level, JS
infile_csv      = 'JAKARTA SELATAN to kelurahan SETIA BUDI od list 20230210 merge keep.csv'
infile  = 'Neighbourhoods_out1.geojson'
outfile = f'{infile.split(".")[0]}_out2.geojson'
#"""

# NOTE!!! Change to use create_step2_geojson_file_from_reversed_od() !!!
""" 2)
kab_kota_origin = 'JAKARTA BARAT'
destination     = 'SETIA BUDI'
infile_csv      = 'JAKARTA SELATAN JAKARTA BARAT od list 20230202 keep.csv'
infile  = 'Neighbourhoods_out1_out2.geojson'
outfile = f'{infile.split(".")[0]}_out3.geojson'
"""

""" 3)
kab_kota_origin = 'JAKARTA UTARA'
destination     = 'SETIA BUDI'
infile_csv      = 'JAKARTA SELATAN JAKARTA UTARA od list 20230206 merge keep.csv'
infile  = 'Neighbourhoods_out1_out2_out3.geojson'
outfile = f'{infile.split(".")[0]}_out4.geojson'
"""

""" 4)
kab_kota_origin = 'JAKARTA TIMUR'
destination     = 'SETIA BUDI'
infile_csv      = 'JAKARTA SELATAN JAKARTA TIMUR od list mod 20230207 keep.csv'
infile  = 'Neighbourhoods_out1_out2_out3_out4.geojson'
outfile = f'{infile.split(".")[0]}_out5.geojson'
"""

header_od = ['O_OBJECTID', 'O_WADMKK', 'O_WADMKC', 'O_WADMKD', 'O_WADMRW', 'O_LAT', 'O_LNG', 'D_OBJECTID', 'D_WADMKK', 'D_WADMKC', 'D_WADMKD', 'D_WADMRW', 'D_LAT', 'D_LNG', 'T transit', 'T driving', 'T ridehail', 'CO2e transit', 'CO2e driving', 'route_details', 'H']


def main():

  create_step2_geojson_file(infile_csv, kab_kota_origin, destination)
  #create_step2_geojson_file_from_reversed_od(infile_csv, kab_kota_origin, destination)

  return



def create_step2_geojson_file(infile_csv, kab_kota_origin, destination):
  infile_csv_rows = 0
  num_of_keys = num_of_features = 0

  features_out = []
  feature_destination = 0

  d = Path(__file__).resolve().parents[1]
  #d = d / 'Data' / 'yourfile.txt'
  d = d / infile_csv

  print(f'Input geojson file: {infile}')
  with open(infile) as f:
    dest_geojs = json.load(f)

  # for n, key in enumerate(dest_geojs, start=1): 
  #   #print(key)
  #   num_of_keys = n

  for n, features in enumerate(dest_geojs['features'], start=1): 
    #print(features)
    num_of_features = n
  #print(f'{len(dest_geojs)} {num_of_keys} {num_of_features}')  # 2 i.e. {'type': 'FeatureCollection', 'features': [] }

  print(f'Input CSV OD  file: {d}')
  with open(d, 'r') as f_in:
    csv_data = reader(f_in)
    header = next(csv_data)

    if header != None:
      for n, row_od in enumerate(csv_data, start=1):       
        #print(f'{n}: {row_od}')
        od = dict(zip(header_od, row_od))

        # For OD lookup,
        # origin is kecamatan level i.e. O_WADMKK field in csv
        if od['O_WADMKK'] == kab_kota_origin:

          # destination is kelurahan level i.e. D_WADMKD field in csv
          if od['D_WADMKD'] == destination: 

            update_geojson_v4(dest_geojs, od, output=True)

            feature_destination += 1

        infile_csv_rows = n

  print('\nSummary:')
  print(f'Input geojson file: {infile} {num_of_features} features.')
  print(f'Input CSV OD  file: {infile_csv}  {infile_csv_rows} rows, {feature_destination} processsed.')

  print(f'Output geojson file: {outfile}')
  with open(f'{outfile}', 'w') as f:
     geojson.dump(dest_geojs, f)

  return

def update_geojson_v4(geojs, od, output):

  idx = feature_count = 0

  kelur_destination = od['D_WADMKD']
  kelur_origin      = od['O_WADMKD']

  for i, feature in enumerate(geojs['features']):
    #print(f'{i}')  # ok, output 0 to x feature in features

    for key, value in feature.items():
      #print(key, value) # ok, output type, properties, geometry

      # Add transport data
      if key == 'properties': #and value['WADMKD'] == kelur_destination:
        if output is True: 
          print('Destination: ', value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'], 'Origin: ', kelur_origin )

        tr = geojs['features'][i]['properties']['T_transit'] = float(od['T transit'])
        td = geojs['features'][i]['properties']['T_driving'] = float(od['T driving'])
        th = geojs['features'][i]['properties']['T_ridehail'] = float(od['T ridehail'])
        geojs['features'][i]['properties']['CO2e_transit'] = float(od['CO2e transit'])
        geojs['features'][i]['properties']['CO2e_driving'] = float(od['CO2e driving'])
        geojs['features'][i]['properties']['route_details'] = od['route_details']

        geojs['features'][i]['properties']['to_destination'] = kelur_destination

        income = geojs['features'][i]['properties']['income']
        tr_per_month = tr * 2 * 30.5  # 2 trips a day, ave 30.5 days a month
        td_per_month = td * 2 * 30.5 
        th_per_month = th * 2 * 30.5 
        pct_tr = geojs['features'][i]['properties']['pct_income_on_T_transit'] = round((tr_per_month/income)*100, 2)   
        pct_td = geojs['features'][i]['properties']['pct_income_on_T_driving'] = round((td_per_month/income)*100, 2) 
        geojs['features'][i]['properties']['pct_income_on_T_ridehail'] = round((th_per_month/income)*100, 2) 

        geojs['features'][i]['properties']['pct_income_on_HnTr_cost'] = \
          geojs['features'][i]['properties']['pct_income_on_H_cost'] + pct_tr

        geojs['features'][i]['properties']['pct_income_on_HnTd_cost'] = \
          geojs['features'][i]['properties']['pct_income_on_H_cost'] + pct_td

        idx += 1 # count keys added

    feature_count = i+1 # as i starts at 0

  print(f'{idx} keys added to from_origin. {feature_count} features.')

  return


def create_step2_geojson_file_from_reversed_od(infile_csv, kab_kota_origin, destination):
  infile_csv_rows = 0
  num_of_keys = num_of_features = 0

  features_out = []
  feature_origin = 0

  d = Path(__file__).resolve().parents[1]
  #d = d / 'Data' / 'yourfile.txt'
  d = d / infile_csv

  print(f'Input geojson file: {infile}')  
  with open(infile) as f:
    orig_geojs = json.load(f)

  # for n, key in enumerate(orig_geojs, start=1): 
  #   #print(key)
  #   num_of_keys = n

  for n, features in enumerate(orig_geojs['features'], start=1): 
    #print(features)
    num_of_features = n
  #print(f'{len(orig_geojs)} {num_of_keys} {num_of_features}')  # 2 i.e. {'type': 'FeatureCollection', 'features': [] }


  print(f'Input CSV OD  file: {d}')
  with open(d, 'r') as f_in:
    csv_data = reader(f_in)
    header = next(csv_data)

    if header != None:
      for n, row_od in enumerate(csv_data, start=1):       
        #print(f'{n}: {row_od}')
        od = dict(zip(header_od, row_od))

        # For reversed OD lookup, 
        # first check the O_WADMKD (kelurahan) field in csv if it's the destination we want
        # then check the D_WAMKK (kecamatan) field in csv if it's the orgin we wan
        if od['O_WADMKD']== destination:

          # destination is kelurahan level i.e. D_WADMKD
          if od['D_WADMKK'] == kab_kota_origin:

            update_geojson_reversed_od(orig_geojs, od, output=True)

            feature_origin += 1

        infile_csv_rows = n

  print('\nSummary:')
  print(f'Input geojson file: {infile} {num_of_features} features.')
  print(f'Input CSV OD  file: {infile_csv}  {infile_csv_rows} rows, {feature_origin} processsed.')


  print(f'Output geojson file: {outfile}')
  with open(f'{outfile}', 'w') as f:
     geojson.dump(orig_geojs, f)

  return


def update_geojson_reversed_od(geojs, od, output):

  idx = feature_count = 0

  kelur_destination = od['D_WADMKD']
  kelur_origin      = od['O_WADMKD'] 

  for i, feature in enumerate(geojs['features']):
    #print(f'{i}')  # ok, output 0 to x feature in features

    for key, value in feature.items():
      #print(key, value) # ok, output type, properties, geometry

      # Add csv info to geojson if kelurahan of geojs matches kelurahan we want from csv
      if key == 'properties' and value['WADMKD'] == kelur_destination:
        if output is True: 
          print('Origin: ', value['WADMKK'], ',', value['WADMKC'], ',', value['WADMKD'], 'Destination: ', kelur_origin )

        tr = geojs['features'][i]['properties']['T_transit'] = float(od['T transit'])
        td = geojs['features'][i]['properties']['T_driving'] = float(od['T driving'])
        th = geojs['features'][i]['properties']['T_ridehail'] = float(od['T ridehail'])
        geojs['features'][i]['properties']['CO2e_transit'] = float(od['CO2e transit'])
        geojs['features'][i]['properties']['CO2e_driving'] = float(od['CO2e driving'])
        geojs['features'][i]['properties']['route_details'] = od['route_details']

        # for reversed OD lookup, the destination is the O_WADMKD field in csv
        geojs['features'][i]['properties']['to_destination'] = kelur_origin 

        income = geojs['features'][i]['properties']['income']
        tr_per_month = tr * 2 * 30.5  # 2 trips a day, ave 30.5 days a month
        td_per_month = td * 2 * 30.5 
        th_per_month = th * 2 * 30.5 
        pct_tr = geojs['features'][i]['properties']['pct_income_on_T_transit'] = round((tr_per_month/income)*100, 2)   
        pct_td = geojs['features'][i]['properties']['pct_income_on_T_driving'] = round((td_per_month/income)*100, 2) 
        geojs['features'][i]['properties']['pct_income_on_T_ridehail'] = round((th_per_month/income)*100, 2)

        geojs['features'][i]['properties']['pct_income_on_HnTr_cost'] = \
          geojs['features'][i]['properties']['pct_income_on_H_cost'] + pct_tr

        geojs['features'][i]['properties']['pct_income_on_HnTd_cost'] = \
          geojs['features'][i]['properties']['pct_income_on_H_cost'] + pct_td


        idx += 1 # count keys added

    feature_count = i+1 # as i starts at 0

  print(f'{idx} keys added to from_origin. {feature_count} features.')

  return

if __name__ == '__main__':
  main()